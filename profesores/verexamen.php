<!DOCTYPE html>
<?php
// Iniciamos sesión y asignamos variables.
session_start();
$matricula = $_SESSION['matricula'];
$matriculaAlumno = $_POST['estudiante'];
$UeaMateria = $_POST['materia'];
$nombreMateria = $_POST['nombre'];

// Si no hay una matrícula válida en este punto, los mandamos a errorlogin
if ($matricula == null) {
	session_unset();
	session_destroy();
	header("Location: /examenuamonline.atwebpages.com/util/errorlogin.html"); //Ruta
	die();
}

// Función para asignar un valor de letra a un número
function asignaLetra($valor) {
	$resultado = 'z';
	switch ($valor) {
		case 1:
		$resultado = 'a';
		break;
		case 2:
		$resultado = 'b';
		break;
		case 3:
		$resultado = 'c';
		break;
		case 4:
		$resultado = 'd';
		break;
	}
	return $resultado;
}
?>

<html>
<head>
	<title>Aula virtual | Examen presentado</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div align = "center">
		<center> <a href="/examenuamonline.atwebpages.com/index.html" > <img src = "/examenuamonline.atwebpages.com/logos/logouam.jpg" alt="Logo UAM" height="117.14" width="400"></a></center> <!--ruta-->
	</div>

	<?php
	require('db_connect.php');

	// Seleccionamos el id del examen presentado
	$queryIdExamen = "SELECT idExamen, fecha FROM inscripciones WHERE estudiante='$matriculaAlumno' AND materia='$UeaMateria'";
	$resultadoIdExamen = mysqli_query($connection,$queryIdExamen) or die(mysqli_error($connection));
	$renglonIdExamen = mysqli_fetch_array($resultadoIdExamen,MYSQLI_NUM);

	// Asiganmos los datos a variables
	$idExamen = $renglonIdExamen[0];
	$fecha = $renglonIdExamen[1];

	echo "<div align=\"center\">";
	echo "<h2> Examen de conocimientos de '$nombreMateria' presentado el '$fecha'</h2>";
	echo "</div>";

	// Seleccionamos las preguntas que le tocaron al alumno
	$queryTipoExamen = "SELECT * FROM vistaPreguntasExamen WHERE idExamen = $idExamen";
	$resultadoTipoExamen = mysqli_query($connection,$queryTipoExamen) or die(mysqli_error($connection));
	$renglonTipoExamen = mysqli_fetch_array($resultadoTipoExamen,MYSQLI_NUM);

	echo "<div align=\"left\">";
	echo "<form action=\"/examenuamonline.atwebpages.com/profesores/panelprofesor.php\" method=\"post\">";

	// Presentamos 10 preguntas
	for ($i=1 ; $i<11 ; $i++) {
		$idPreguntaActualExamen = intval($renglonTipoExamen[$i]);
		$queryPreguntaActual = "SELECT * FROM preguntas WHERE idPregunta=$idPreguntaActualExamen AND idMateria='$UeaMateria'";
		$resultadoPreguntaActual = mysqli_query($connection,$queryPreguntaActual) or die(mysqli_error($connection));
		$renglonPreguntaActual = mysqli_fetch_array($resultadoPreguntaActual,MYSQLI_NUM);
		echo "<p>";
		echo "<h4> Pregunta $i </h4>";
		echo "$renglonPreguntaActual[2]";
		echo "\n<br>";

		//Determinamos la respuesta correcta.
		$queryRespuestaPregunta = "SELECT respuesta FROM preguntas WHERE idMateria = '$UeaMateria' AND idPregunta = '$renglonTipoExamen[$i]'";
		$resultadoRespuestaPregunta = mysqli_query($connection,$queryRespuestaPregunta) or die(mysqli_error($connection));
		$renglonRespuestaPregunta = mysqli_fetch_array($resultadoRespuestaPregunta,MYSQLI_NUM);
		$respuestaCorrecta = $renglonRespuestaPregunta[0];

		//Determinamos la respuesta del alumno
		$queryRespuestasAlumno = "SELECT * FROM vistaRespuestasExamen WHERE idExamen = $idExamen";
		$resultadoRespuestasAlumno = mysqli_query($connection,$queryRespuestasAlumno) or die(mysqli_error($connection));
		$renglonRespuestasAlumno = mysqli_fetch_array($resultadoRespuestasAlumno,MYSQLI_NUM);
		$respuestaAlumno = $renglonRespuestasAlumno[$i];

		// Desplegamos la respuestas posibles
		for ($j=3;$j<7;$j++) {
			$value = $j-2;
			if (asignaLetra($value) == $respuestaCorrecta) {
				$estilo = "correcto-label";
			} else {
				$estilo = "incorrecto-label";
			}
			if (asignaLetra($value) == $respuestaAlumno) {
				$default = "checked";
			} else {
				$default = "";
			}
			echo "<input type=\"radio\" value=\"$value\" name=\"pregunta-$i\" disabled $default>
			<label for=\"pregunta-$i\" id=\"$estilo\">$renglonPreguntaActual[$j]</label>";
		}
		echo "</p>";
	}
	echo '<input type="submit" value="Volver al panel de control">';
	echo "</form>";
	echo "</div>";
	?>

</body>
</html>
