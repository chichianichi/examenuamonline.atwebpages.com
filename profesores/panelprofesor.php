<!DOCTYPE html>
<?php
// Iniciamos la sesión
session_start();
?>
<html>
<head>
	<title>Aula virtual | Panel</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<div align = "center">
		<center> <a href="/index.html" > <img src = "/examenuamonline.atwebpages.com/logos/logouam.jpg" alt="Logo UAM" height="117.14" width="400"></a></center> <!-- rura-->
	</div>

	<div <div align="center">
		<h1>Panel de administración para profesores</h1>
	</div>

	<div align = "center">
		<?php
		require('db_connect.php');
		$matricula = $_SESSION['matricula'];

		// Si no hay una matrícula válida en este punto, los mandamos a errorlogin
		if ($matricula == null) {
			session_unset();
			session_destroy();
			header("Location: /examenuamonline.atwebpages.com/util/errorlogin.html"); //ruta
			die();
		}

		// Pasamos la matrícula a la siguiente sesión
		$_SESSION['matricula'] = $matricula;



		$queryCuentaAlumnos = "SELECT COUNT(*) FROM datosAlumno WHERE tutor = '$matricula'";
		$resultadoCuentaAlumnos= mysqli_query($connection, $queryCuentaAlumnos) or die(mysqli_error($connection));
		$renglonCuentaAlumnos = mysqli_fetch_array($resultadoCuentaAlumnos, MYSQLI_NUM);
		$cuentaAlumnos = $renglonCuentaAlumnos[0];

		if ($cuentaAlumnos > 0) {
			// Seleccionamos los alumnos para los cuales el profesor es tutor
			$query = "SELECT * FROM datosAlumno WHERE tutor = '$matricula'";
			$result = mysqli_query($connection, $query) or die(mysqli_error($connection));


			echo "<h2>Alumnos asignados</h2>";

			// Desplegamos los alumnos
			echo '<table><tr> <th id="panel-th">Matricula Alumno</th> <th id="panel-th">Nombre Alumno</th><th id="panel-th">Ver información</th></tr>';
			while ($renglon=mysqli_fetch_array($result,MYSQLI_NUM)) {
				$matriculaAlumno = $renglon[0];
				$nombreAlumno = $renglon[1];
				$apellidoAlumno = $renglon[2];
				$nombreAlumno = $nombreAlumno . " " . "$apellidoAlumno";
				echo "<tr>";
					echo "<td align=\"center\" id=\"panel-td\">".$matriculaAlumno."</td>";
					echo "<td align=\"center\" id=\"panel-td\">".$nombreAlumno."</td>";
					echo "<td align=\"center\" id=\"panel-td\"><form method=\"post\" action=\"/examenuamonline.atwebpages.com/profesores/accionpanel.php\"> 
					<input name=\"matriculaAlumno\" type=\"hidden\" value=\"$matriculaAlumno\">
					<input name=\"submit\" type=\"submit\" value=\"Detalles\">
					</form></td></tr>";  //ruta
			}
			echo "</table>";
		}
		else {
			echo "<div align=\"center\">";
			echo "<h2> No tiene alumnos asignados </h2>";
			echo "</div>";
		}


		?>
	</div>

	<div class="footer">
		<p>Aula virtual UAM-I</p>
		<a href="/examenuamonline.atwebpages.com/profesores/logout.php"><font color="FFFFFF">Salir de la sesión</font> </a>
		<br><br>
	</div>
</body>
</html>
