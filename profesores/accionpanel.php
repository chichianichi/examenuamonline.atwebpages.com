<!DOCTYPE html>
<?php
	// Iniciamos sesión y asignamos variables.
	session_start();
	$matricula = $_SESSION['matricula'];
	$matriculaAlumno = $_POST['matriculaAlumno'];
	$_SESSION['matricula'] = $matricula;
	// Si no hay una matrícula válida en este punto, los mandamos a errorlogin
	if ($matricula == null) {
		session_unset();
		session_destroy();
		header("Location: /examenuamonline.atwebpages.com/util/errorlogin.html"); //Ruta
		die();
	}
?>
<html>
	<head>
		<title>Aula virtual | Información Alumno</title>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>

	<body>
		<div align = "center">
			<center> <a href="/examenuamonline.atwebpages.com/index.html" > <img src = "/examenuamonline.atwebpages.com/logos/logouam.jpg" alt="Logo UAM" height="117.14" width="400"></a></center> <!--Ruta -->
		</div>

		<?php
		require('db_connect.php');
		// Contamos las materias que tiene el alumno
		$queryCantidadMateriasAlumno = "SELECT COUNT(materia) FROM inscripciones WHERE estudiante = '$matriculaAlumno' AND idExamen IS NOT NULL";
		$cantidadMateriasAlumno = mysqli_query($connection,$queryCantidadMateriasAlumno) or die(mysqli_error($connection));
		$renglonCantidadMateriasAlumno = mysqli_fetch_array($cantidadMateriasAlumno,MYSQLI_NUM);
		$cantidadMaterias = $renglonCantidadMateriasAlumno[0];
		if(($cantidadMaterias > 0)) {
			// Si tiene más de 0 mostramos un resumen
			echo "<div align=\"center\">";
			echo "<h2> Evaluaciones hechas por el alumno </h2>";


			$query = "SELECT DISTINCT m.nombre, (CASE
				WHEN i.idExamen IS NULL THEN 'Falta presentar examen'
				ELSE e.calificacion
				END) AS calificacion, i.estudiante, i.materia
				FROM inscripciones i, examenes e, materias m
				WHERE i.estudiante = '$matriculaAlumno'
				AND i.estudiante = e.estudiante
				AND i.idExamen = e.idExamen
				AND i.materia = m.idMateria";

				$result = mysqli_query($connection, $query) or die(mysqli_error($connection));

				// Mostramos todas las materias para las cuales ha presentado exámenes
				echo '<table><tr> <th id="panel-th">Materia</th> <th id="panel-th">Calificación</th><th id="panel-th">Examen</th></tr>';
				while ($renglon=mysqli_fetch_array($result,MYSQLI_NUM))
				{
					$nombre = $renglon[0];
					$calificacion = $renglon[1];
					$estudiante = $renglon[2];
					$materia= $renglon[3];

					$valorBoton = "Ver examen";

					echo "<tr>";
					echo "<td align=\"center\" id=\"panel-td\">".$nombre."</td>";
					echo "<td align=\"center\" id=\"panel-td\">".$calificacion."</td>";
					echo "<td align=\"center\" id=\"panel-td\"><form method=\"post\" action=\"verexamen.php\">
					<input name=\"nombre\" type=\"hidden\" value=\"$nombre\">
					<input name=\"estudiante\" type=\"hidden\" value=\"$estudiante\">
					<input name=\"materia\" type=\"hidden\" value=\"$materia\">
					<input name=\"submit\" type=\"submit\" value=\"$valorBoton\">
					</form></td></tr>";
				}
				echo "</table><br>";
				echo '<button onclick="location.href = \'/examenuamonline.atwebpages.com/profesores/panelprofesor.php\';" id="boton-regresar">Regresar al panel</button>'; //Ruta
				echo "</div>";
			} else {
				// De otra manera no mostramos nada
				echo "<div align=\"center\">";
				echo "<h2> El alumno no tiene evaluaciones realizadas </h2>";
				echo '<button onclick="location.href = \'/examenuamonline.atwebpages.com/profesores/panelprofesor.php\';" id="boton-regresar">Regresar al panel</button>'; //Ruta
				echo "</div>";
			}
			?>
			<div class="footer">
				<p>Aula virtual UAM-I</p>
				<a href="logout.php"><font color="FFFFFF">Salir de la sesión</font> </a>
				<br><br>
			</div>

		</body>
</html>
