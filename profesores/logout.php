<!DOCTYPE html>
<html>
<body>
    <?php
    // Destruimos la sesión, borramos la info y los mandamos al index
    session_start();
    session_unset();
    session_destroy();
    header("Location: /examenuamonline.atwebpages.com/index.html");
    die();
    ?>
</body>
</html>
