# Examen UAM en Linea

<h2>Proyecto de Temas Selectos de Bases de Datos</h2>

<p>
    Proyecto de simulación para el desarrollo de una página WEB en dónde se podran realizar exámenes,<br>
    Tenemos dos tipos de usuarios, el usuario "Profesor" y "Alumno". En la vista de los alumnos podemos <br>
    visualizar las materias que están dadas de alta, las cuales podremos presentar un examen con preguntas <br>
    elegidas aleatoriamente de la base de datos y cuando el alumno de por finalizado el examen se guardarán las <br>
    respuestas.<br>
    La vista de Profesores nos dejará ver los alumnos que tiene asignados y vamos a poder visualizar los <br>
    exámenes de los alumnos. 
</p>

<h3>Instrucciones</h3>
<p>
    Para ejecutar el proyecto sin inconvenientes, se debe de seguir los siguientes pasos:    
</p>
<ul>
    <li>
        Tener Xampp instalado. 
    </li>
    <li>
        Por medio de Xampp, ejecutar el código o el archivo "20190702_SqlProyecto_A.sql" que corresponde a la Base de Datos empleada.
    </li>
    <li>
        Tener la carpeta del proyecto dentro de la carpeta de Xampp en la siguiente ruta: "xampp\htdocs" <br>
        o en su defecto configurar Xampp para que el localhost inicie la carpeta del proyecto
    </li>
    <li>
        Para poder visualizar la pagina WEB debemos tener iniciado los servicios:
        <ul>
            <li>Apache</li>
            <li>MySQL</li>
        <ul>        
    </li>
    <li>
        Ingresar a la la dirección (http://localhost/examenuamonline.atwebpages.com)
    </li>
</ul>

<h3>Accesos del sistema</h3>
<p>
    La navegación de la página es muy dinámica y no necesita explicación, pero para poder hacer de su uso <br>
    hay usuarios de prueba creados los cuales requieren username y password, Los cuales son los siguientes:
</p>
<h5>Profesores</h5>
<ul>
    <li>Usuario: 999 <br> Contraseña: pass</li>
    <li>Usuario: 998 <br> Contraseña: pass</li>
    <li>Usuario: 199532071 <br> Contraseña: pass</li>
    <li>Usuario: 138579691 <br> Contraseña: pass</li>
    <li>Usuario: 983620701 <br> Contraseña: 0000 </li>
</ul>

<h5>Alumnos</h5>
<ul>
    <li>Usuario: 216333800 <br> Contraseña: pass</li>
    <li>Usuario: 216338900 <br> Contraseña: pass</li>
    <li>Usuario: 216333897 <br> Contraseña: pass</li>
    <li>Usuario: 216338879 <br> Contraseña: 1234</li>
    <li>Usuario: 216257913 <br> Contraseña: 0000 </li>
</ul>