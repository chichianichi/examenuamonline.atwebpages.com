<?php
session_start();
?>

<!DOCTYPE html>



<?php
// Iniciamos sesión y asignamos variables.

$matriculaEstudiante = $_SESSION['matriculaEstudiante'];
$UeaMateria = $_SESSION['UeaMateria'];
$idExamen = $_SESSION['idExamen'];
printf($idExamen);
// Si no hay una matrícula válida en este punto, los mandamos a errorlogin
if ($matriculaEstudiante == null) {
	session_unset();
	session_destroy();
	header("Location: /util/errorlogin.html");
	die();
}
require('db_connect.php');
// Función para asignar un valor de letra a un número
function asignaLetra($valor) {
    $resultado = 'z';
    switch ($valor) {
        case 1:
        $resultado = 'a';
        break;
        case 2:
        $resultado = 'b';
        break;
        case 3:
        $resultado = 'c';
        break;
        case 4:
        $resultado = 'd';
        break;
    }
    return $resultado;
}

// Inicializamos la calificación en 0
$calif = 0;

// Obtenemos las preguntas del examen
$queryTipoExamen = "SELECT * FROM vistaPreguntasExamen WHERE idExamen = '$idExamen'";
$resultadoTipoExamen = mysqli_query($connection,$queryTipoExamen) or die(mysqli_error($connection));
$renglonTipoExamen = mysqli_fetch_array($resultadoTipoExamen,MYSQLI_NUM);

// Para cada pregunta que se presentó en el examen, comparamos la que nos pasaron con la correcta
for ($i = 1 ; $i < 11 ; $i++) {
    // Buscamos las respuestas
    $queryRespuestaPregunta = "SELECT respuesta FROM preguntas WHERE idMateria = '$UeaMateria' AND idPregunta = '$renglonTipoExamen[$i]'";
    $resultadoRespuestaPregunta = mysqli_query($connection,$queryRespuestaPregunta) or die(mysqli_error($connection));
    $renglonRespuestaPregunta = mysqli_fetch_array($resultadoRespuestaPregunta,MYSQLI_NUM);
    $respuestaCorrecta = $renglonRespuestaPregunta[0];

    // Workaround para la consulta de la pregunta de la página anterior
    $cadenaConsulta = "pregunta-$i";
    $respuesta = $_POST[$cadenaConsulta];
    $respuestaAlumno = asignaLetra($respuesta);

    // Si la respuesta es correcta, sube un punto
    if ($respuestaAlumno == $respuestaCorrecta) {
        $calif++;
    }

    // Guardamos las respuestas del alumno también
    $cadenaInsertaRespuestasIntentoExamen = "UPDATE examenes SET respuesta$i = '$respuestaAlumno' WHERE idExamen = '$idExamen'";
    mysqli_query($connection,$cadenaInsertaRespuestasIntentoExamen) or die (mysqli_error($connection));
}

// Colocamos la calificación obtenida
$queryActualizaCalif = "UPDATE examenes SET calificacion = $calif WHERE idExamen = $idExamen";
mysqli_query($connection,$queryActualizaCalif) or die (mysqli_error($connection));

// Los mandamos de regreso al panel
header("Location: /examenuamonline.atwebpages.com/alumnos/panelalumno.php");
die();
?>
