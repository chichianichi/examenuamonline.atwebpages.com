
<!DOCTYPE html>
<?php
// Iniciamos la sesión
session_start();
?>

<html>
<head>
	<title>Aula virtual |Panel</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div align = "center">
		<center> <a href="/index.html" > <img src = "/examenuamonline.atwebpages.com/logos/logouam.jpg" alt="Logo UAM" height="117.14" width="400"></a></center>
	</div>
	<div <div align="center">
		<h1>Panel de administración para alumnos</h1>
	</div>

	<div align = "center"><h2>Materias inscritas</h2>
	</div>

	<div align = "center">

		<?php
		
        require('db_connect.php');
		$matricula = $_SESSION['matricula'];
		//Si no hay una matrícula válida en este punto, los mandamos a errorlogin
		if ($matricula == null) {
			session_unset();
			session_destroy();
			header("Location: /util/errorlogin.html");
			die();
		}

		// Contamos en cuantas materias está inscrito el alumno
		$consultaCuenta = "SELECT COUNT(*) FROM inscripciones WHERE estudiante = '$matricula' AND idExamen IS NOT NULL;";
		$resultadoCuenta = mysqli_query($connection, $consultaCuenta) or die(mysqli_error($connection));
		$renglonResultadoCuenta=mysqli_fetch_array($resultadoCuenta,MYSQLI_NUM);

		// Si tiene materias mixtas usamos una query
		if ($renglonResultadoCuenta[0]!=0) {
			$query = "SELECT DISTINCT m.nombre, (CASE
				WHEN i.idExamen IS NULL THEN 'Falta presentar examen'
				ELSE e.calificacion
				END) AS calificacion, i.estudiante, i.materia
				FROM inscripciones i, examenes e, materias m
				WHERE i.estudiante = '$matricula'
				AND i.estudiante = e.estudiante
				AND (i.idExamen = e.idExamen OR i.idExamen IS NULL)
				AND i.materia = m.idMateria";
			} else {
				// De otra manera usamos esta
				$query = "SELECT m.nombre, 'Falta presentar examen' AS calificacion, i.estudiante, i.materia
				FROM inscripciones i, materias m
				WHERE i.estudiante = '$matricula'
				AND i.materia = m.idMateria";
			}
			$result = mysqli_query($connection, $query) or die(mysqli_error($connection));

			// Mostramos la tabla con la información de las materias
			echo '<table><tr> <th id="panel-th">Materia</th> <th id="panel-th">Calificación</th><th id="panel-th">Acción disponible</th></tr>';
			while ($renglon=mysqli_fetch_array($result,MYSQLI_NUM))
			{
				$nombre = $renglon[0];
				$calificacion = $renglon[1];
				$estudiante = $renglon[2];
				$materia= $renglon[3];
				// Desplegamos el texto del botón acorde a si ya lo presentó o no
				if ($calificacion == "Falta presentar examen") {
					$valorBoton = "Aplicar Examen";
				}
				else {
					$valorBoton = "Ver detalles";
				}

				echo "<tr>";
				echo "<td align=\"center\" id=\"panel-td\">".$nombre."</td>";
				echo "<td align=\"center\" id=\"panel-td\">".$calificacion."</td>";
				echo "<td align=\"center\" id=\"panel-td\"><form method=\"post\" action=\"accionpanel.php\">
				<input name=\"nombre\" type=\"hidden\" value=\"$nombre\">
				<input name=\"calificacion\" type=\"hidden\" value=\"$calificacion\">
				<input name=\"estudiante\" type=\"hidden\" value=\"$estudiante\">
				<input name=\"materia\" type=\"hidden\" value=\"$materia\">
				<input name=\"submit\" type=\"submit\" value=\"$valorBoton\">
				</form></td></tr>";
			}
			echo "</table>";
			?>
		</div>

		<div class="footer">
			<p>Aula virtual UAM-I</p>
			<a href="/examenuamonline.atwebpages.com/alumnos/logout.php"><font color="FFFFFF">Salir de la sesión</font> </a>
			<br><br>
		</div>
	</body>
	</html>
