<?php
session_start();
?>

<!DOCTYPE html>
<script>
// Script para timeout
setTimeout(function(){document.getElementById('submit-form').submit();}, 3600000);

// Script para advertir si intenta dejar la página
window.addEventListener('beforeunload', function(e) {
  var myPageIsDirty = isset($fecha);
  if(myPageIsDirty) {
    //following two lines will cause the browser to ask the user if they
    //want to leave. The text of this dialog is controlled by the browser.
    e.preventDefault(); //per the standard
    e.returnValue = ''; //required for Chrome
  }
});
</script>
<?php
// Iniciamos sesión y asignamos variables.

$nombreMateria = $_POST ['nombre'];
$calificacion = $_POST ['calificacion'];
$matriculaEstudiante = $_POST ['estudiante'];
$UeaMateria = $_POST ['materia'];

// Si no hay una matrícula válida en este punto, los mandamos a errorlogin
if ($matriculaEstudiante == null) {
	session_unset();
	session_destroy();
	header("Location: /examenuamonline.atwebpages.com/util/errorlogin.html");
	die();
}
?>
<html>

<head>
<?php
	if ($calificacion == "Falta presentar examen") {
		echo "<title>Aula virtual | Examen</title>";
		$presentandoExamen = True;
	}
	else {
		echo "<title>Aula virtual | Información Examen</title>";
		$presentandoExamen = False;
	}
?>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div align = "center">
		<center> <a href="/examenuamonline.atwebpages.com/index.html" > <img src = "/examenuamonline.atwebpages.com/logos/logouam.jpg" alt="Logo UAM" height="117.14" width="400"></a></center>
	</div>

	<?php
		require('db_connect.php');
		if ($presentandoExamen) {

			// Buscamos el nombre y apellido del alumno
			$queryNombreAlumno = "SELECT apellido, nombre FROM estudiantes WHERE matricula = '$matriculaEstudiante'"; 
			$resultadoNombreAlumno = mysqli_query($connection,$queryNombreAlumno) or die(mysqli_error($connection));
			$renglonNombreAlumno = mysqli_fetch_array($resultadoNombreAlumno,MYSQLI_NUM);

			// Guardamos la fecha del día de hoy
			$fecha = date('Y-m-d H:i:s');

			// Creamos un examen
			$cadenaInsertaExamen = "INSERT INTO examenes (fecha, estudiante, idMateria) VALUES ('$fecha','$matriculaEstudiante','$UeaMateria')";
			mysqli_query($connection,$cadenaInsertaExamen) or die (mysqli_error($connection));

			// Se actualiza automáticamente en las inscripciones el ID del Examen nuevo

			// Obtenemos el ID del Examen nuevo
			$cadenaObtenIdExamen = "SELECT idExamen FROM inscripciones WHERE estudiante = '$matriculaEstudiante' AND materia = '$UeaMateria'";
			$resultadoIdExamen = mysqli_query($connection,$cadenaObtenIdExamen) or die(mysqli_error($connection));
			$renglonIdExamen = mysqli_fetch_array($resultadoIdExamen,MYSQLI_NUM);

			// Asignamos el idExamen a una variable
			$idExamen = $renglonIdExamen[0];

			// Creamos 10 preguntas aleatorias para el alumno y las metemos a la tabla
			$queryCuentaPreguntasPorUea = "SELECT COUNT(pregunta) FROM preguntas WHERE idMateria = '$UeaMateria'";
			$resultadoCuentaPreguntasPorUea = mysqli_query($connection,$queryCuentaPreguntasPorUea) or die(mysqli_error($connection));
			$renglonCuentaPreguntas = mysqli_fetch_array($resultadoCuentaPreguntasPorUea,MYSQLI_NUM);

			// Definimos el número de preguntas por examen
			$numPreguntas = 10;

			// Vemos cuantas preguntas hay de la materia
			$preguntasExistentes = $renglonCuentaPreguntas[0];

			// Inicializamos y llenamos un vector de números aleatorios entre 1 y el número de preguntas existentes
			for($r=0;$r<$numPreguntas;$r++) $vectorAleatorio[$r]=0;
			for($r=0;$r<$numPreguntas;$r++){
				$alea=rand(1,$preguntasExistentes);
				$bandera=true;
				for($f=0;$f<$r;$f++)
				if($vectorAleatorio[$f]==$alea){
					$bandera=false;
					break;
				}
				if(!$bandera){ $r--; continue; }
				$vectorAleatorio[$r]=$alea;
			}

			// Guardamos el vector aleatorio elemento a elemento en las preguntas
			for ($i=0;$i<10;$i++) {
				$j = $i+1;
				$cadenaInsertaPreguntasExamen = "UPDATE examenes SET pregunta$j = $vectorAleatorio[$i] WHERE idExamen = '$idExamen'";
				mysqli_query($connection,$cadenaInsertaPreguntasExamen) or die (mysqli_error($connection));
			}

			// Presentamos el título
			echo "<div align=\"center\">";
			echo "<h2> Examen de conocimientos de $nombreMateria</h2>";
			echo "<h3>Bienvenido alumno $renglonNombreAlumno[0] $renglonNombreAlumno[1]</h3>";
			echo "<p>Cuentas con 60 minutos para resolver el examen. Si abandonas la página o la recargas el examen será anulado.</p><br>";
			echo "</div>";

			// Ya construyendo el examen en la página.
			$queryTipoExamen = "SELECT * FROM vistaPreguntasExamen WHERE idExamen = '$idExamen'";
			$resultadoTipoExamen = mysqli_query($connection,$queryTipoExamen) or die(mysqli_error($connection));
			$renglonTipoExamen = mysqli_fetch_array($resultadoTipoExamen,MYSQLI_NUM);

			echo "<div align=\"left\">";
			echo "<form action=\"/examenuamonline.atwebpages.com/alumnos/califica.php\" method=\"post\" id=\"submit-form\">";

			// Presentamos 10 preguntas
			for ($i=1 ; $i<11 ; $i++) {
				$idPreguntaActualExamen = intval($renglonTipoExamen[$i]);
				$queryPreguntaActual = "SELECT * FROM preguntas WHERE idPregunta=$idPreguntaActualExamen AND idMateria='$UeaMateria'";
				$resultadoPreguntaActual = mysqli_query($connection,$queryPreguntaActual) or die(mysqli_error($connection));
				$renglonPreguntaActual = mysqli_fetch_array($resultadoPreguntaActual,MYSQLI_NUM);
				echo "<p>";
				echo "<h4> Pregunta $i</h4>";
				echo "$renglonPreguntaActual[2]";
				echo "\n<br>";

				// Cada una con 4 opciones.
				for ($j=3;$j<7;$j++) {
					//Pasamos 10 respuestas a la página que sigue, con el nombre pregunta-$i y los valores 1-4.
					$value = $j-2;
					echo "<input type=\"radio\" value=\"$value\" name=\"pregunta-$i\" > $renglonPreguntaActual[$j]";
				}
				echo "</p>";
			}
			echo '<input type="submit" value="Finalizar Examen">';
			echo "</form>";
			echo "</div>";

			//Pasando las variables con sesiones a la siguiente página:
			$_SESSION['matriculaEstudiante'] = $_POST ['estudiante'];
			$_SESSION['UeaMateria'] = $_POST ['materia'];
			$_SESSION['idExamen'] = $idExamen;

			// Revisamos que sea la primera vez que ve el examen
			if (!isset($_SESSION["visits"]))
					$_SESSION["visits"] = 0;
				$_SESSION["visits"] = $_SESSION["visits"] + 1;

				if ($_SESSION["visits"] > 1)
				{
					$queryActualizaCalif = "UPDATE examenes SET calificacion = 0 WHERE idExamen = $idExamen";
					mysqli_query($connection,$queryActualizaCalif) or die (mysqli_error($connection));
					header("Location: /examenuamonline.atwebpages.com/alumnos/panelalumno.php");
					die();
				}
				else
				{
					// 
				}
		}
		else {
			// El alumno no va a presentar examen, solo mostramos información del examen que se presentó antes.
			$query = "SELECT fecha FROM examenes WHERE estudiante = '$matriculaEstudiante' AND idMateria = $UeaMateria";
			$result = mysqli_query($connection,$query) or die(mysqli_error($connection));
			$renglon = mysqli_fetch_array($result,MYSQLI_NUM);
			echo "<div align=\"center\">";
			echo "<h2> Información del examen </h2>";
			echo '<table><tr> <th id="panel-th">Código UEA</th> <th id="panel-th">Calificación</th><th id="panel-th">Fecha</th></tr>';
			echo "<tr>";
			echo "<td align=\"center\" id=\"panel-td\">".$UeaMateria."</td>";
			echo "<td align=\"center\" id=\"panel-td\">".$calificacion."</td>";
			echo "<td align=\"center\" id=\"panel-td\">".$renglon[0]."</td>";
			echo "</tr></table><br>";
            echo '<button onclick="location.href = \'/examenuamonline.atwebpages.com/alumnos/panelalumno.php\';" id="boton-regresar">Regresar al panel</button>';
			echo "</div>";
			echo "<div class=\"footer\">";
			echo "<p>Aula virtual UAM-I</p>";
			echo "<a href=\"logout.php\"><font color=\"FFFFFF\">Salir de la sesión</font> </a>";
			echo"<br><br>";
			echo "</div>";
		}
	?>
</body>
